FROM openjdk:8-jdk-alpine
MAINTAINER Fabio Callata "fcallata@gmail.com"
WORKDIR /usr/bin
COPY ./target/consultorioweb-0.0.1-SNAPSHOT.jar consultorioweb.jar
EXPOSE 8080
CMD ["java", "-Dspring.profiles.active=docker", "-jar", "consultorioweb.jar"]