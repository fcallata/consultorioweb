package com.fcallata.consultorioweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsultoriowebApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsultoriowebApplication.class, args);
    }

}
