package com.fcallata.consultorioweb.api;

import com.fcallata.consultorioweb.model.dominio.Usuario;
import com.fcallata.consultorioweb.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @GetMapping("/usuarios")
    public List<Usuario> listaUsuario(){

        return usuarioService.getAll();
    }
}
