package com.fcallata.consultorioweb.model.dao;

import com.fcallata.consultorioweb.model.dominio.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioDAOImpl extends JpaRepository<Usuario, Long> {
}
