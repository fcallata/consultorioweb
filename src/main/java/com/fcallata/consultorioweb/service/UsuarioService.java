package com.fcallata.consultorioweb.service;

import com.fcallata.consultorioweb.model.dominio.Usuario;

import java.util.List;

public interface UsuarioService {

    public List<Usuario> getAll();
}
