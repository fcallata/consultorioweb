package com.fcallata.consultorioweb.service.impl;

import com.fcallata.consultorioweb.model.dao.UsuarioDAOImpl;
import com.fcallata.consultorioweb.model.dominio.Usuario;
import com.fcallata.consultorioweb.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsuarioServiceImpl implements UsuarioService {

    @Autowired
    private UsuarioDAOImpl usuarioDAO;

    @Override
    public List<Usuario> getAll() {
        return usuarioDAO.findAll();
    }
}
